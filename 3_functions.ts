// functions

// if dont pass data type of para it will consider as "any" and can make same problem as js 

function addTwo(num:number){
    return num + 2
}

addTwo(2)


function changecase(val:string){
    return val.toUpperCase()
}

changecase("hi")

function signUpUser(name:string,email:string,isPaid:boolean){

}

signUpUser("ruchi","fgas@gbjh.",true)

//arrow function
let login =(name:string,email:string,isPaid:boolean) => {

}

// default values in arraw function

let loginUser =(name:string, email:string,isPaid:boolean = false) => {

}



// better way to write function
// to alo explicitely mention the return type
function add(num) {
    return num + 2;
}
add(2);
//in arrow function
var getname = function (s) {
    return "";
};

//ex
const hero =['thor','spiderman']

// hero.map((hero:string)=>{
//     return `hero is ${hero}`
// })

//here dont need to mentiion as string becoz arr is tring so input will also be string
// hero.map((hero)=>{
//     return `hero is ${hero}`
// })

//but return type is string only
hero.map((hero):string=>{
    return `hero is ${hero}`
})


// return void
//when u r not returning anything
function adding(num:number):void {
   console.log(num + 2) ;
}



// return never
function errHandler(errmsg:string):never{
    throw new Error(errmsg);
}


export {}




