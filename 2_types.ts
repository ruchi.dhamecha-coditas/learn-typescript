// types of ts -
//     Data Types
//     · Boolean 
//     · String 
//     · Number 
//     · Array 
//     · Tuples 
//     · Function 
//     · Enum

//string
const name:string = "ruchi";
console.log(name);

//number
let userid : number = 12.33;

//boolean
let isloggedin :boolean = true;

// any 
//the situation where we cannot gues the data type for value 
// we use any 
let hero;

function getHero(){
    // return "thor"
    // return true
    // return 0
}

hero = getHero() // so the value of hero caan be anything so as datatype so it is "any"

export{}