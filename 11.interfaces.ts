interface Users {
    readonly dbId: number
    email: string,
    userId: number,
    googleId?: string
    // startTrail: () => string
    startTrail(): string
    getCoupon(couponname: string, value: number): number
}

interface Users {
    githubToken: string
}

interface Admin extends Users {
    role: "admin" | "ta" | "learner"
}

const ruchi: Admin = { dbId: 22, email: "h@h.com", userId: 2211,
role: "admin",
githubToken: "github",
startTrail: () => {
    return "trail started"
},
getCoupon: (name: "ruchi10", off: 10) => {
    return 10
}
}
ruchi.email = "h@hc.com"
// ruchi.dbId = 33