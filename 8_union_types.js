var score = 33;
//can be string or number
score = "33";
function getname(name) {
    console.log("db id is : ".concat(name));
    // name.toUpperCase()
    // this is not posisble bcoz new datatype number / string has created
    // so do,
    if (name === "string") {
        name.toUpperCase();
    }
}
//for array
var data = ["1", "2"];
// either all elemenets are strings or all are number
// not some no.s some strings
// to add many values of different data types
var data2 = [1, "2", true];
/// when we want same value
var pi = 3.14;
