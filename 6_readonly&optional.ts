//readonly
//we can not change the value of readonly once it is initialized

//optional
//when certian variable is not mandatorily useful we write ?

type User={
    //readonly
    readonly _id : string
    name:string
    email:string
    isActive:boolean

    //optional
    credcard? : number
}

let myUser:User={
    _id :"1224",
    name:"sn",
    email:"xagbj@.",
    isActive:false

}

//myUser._id="3" not allowed


type cardNum ={
    cardNum :string
}

type cardDate = {
    cardDate:string
}

type cardDetails = cardNum & cardDate & {
    s
}