"use strict";
// functions
Object.defineProperty(exports, "__esModule", { value: true });
// if dont pass data type of para it will consider as "any" and can make same problem as js 
function addTwo(num) {
    return num + 2;
}
addTwo(2);
function changecase(val) {
    return val.toUpperCase();
}
changecase("hi");
function signUpUser(name, email, isPaid) {
}
signUpUser("ruchi", "fgas@gbjh.", true);
//arrow function
var login = function (name, email, isPaid) {
};
// default values in arraw function
var loginUser = function (name, email, isPaid) {
    if (isPaid === void 0) { isPaid = false; }
};
// better way to write function
// to alo explicitely mention the return type
function add(num) {
    return num + 2;
}
add(2);
//in arrow function
var getname = function (s) {
    return "";
};
//ex
var hero = ['thor', 'spiderman'];
// hero.map((hero:string)=>{
//     return `hero is ${hero}`
// })
//here dont need to mentiion as string becoz arr is tring so input will also be string
// hero.map((hero)=>{
//     return `hero is ${hero}`
// })
//but return type is string only
hero.map(function (hero) {
    return "hero is ".concat(hero);
});
