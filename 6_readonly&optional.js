//readonly
//we can not change the value of readonly once it is initialized
var myUser = {
    _id: "1224",
    name: "sn",
    email: "xagbj@.",
    isActive: false
};
