let score:number | string  =33

//can be string or number

score = "33"

function getname(name: number | string){
    console.log(`db id is : ${name}`);

   // name.toUpperCase()
   // this is not posisble bcoz new datatype number / string has created
   // so do,
   if (name === "string"){
    name.toUpperCase()
   }
}


//for array

const data: string[] | number[] = ["1","2"]
// either all elemenets are strings or all are number
// not some no.s some strings

// to add many values of different data types
const data2:(string | number | boolean)[] = [1,"2",true]



/// when we want same value
const pi : 3.14 = 3.14